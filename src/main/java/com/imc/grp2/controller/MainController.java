package com.imc.grp2.controller;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping("/api/v1/main")
public class MainController {
    @RequestMapping(value = "/ok", method = RequestMethod.GET, produces = "application/json;charset=utf-8")
    @ResponseStatus(HttpStatus.OK)
    public Object currentStatus() throws Exception {
        return " Hello world ^^ ";
//        return " is ok ^^ " + URLDecoder.decode("น้ำตาลมิตรผล", "UTF-8");
    }
}
